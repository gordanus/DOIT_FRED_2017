    function proveriUnos() {
        var email = document.getElementById('email').value;
        if (email.length == 0) {
            throw new Error("Molimo unesite email adresu"); //izazvati gresku sa tekstom 'Molimo unesite email adresu'
        } else if (email.indexOf('@') === -1) {
            throw new Error("Molimo unesite validnu email adresu"); //izazvati gresku sa tekstom 'Molimo unesite validnu email adresu
        }
        document.getElementById('console').innerHTML = "<h3 style='color:green'> Your email is: </h3>" + email;
    }

    function validirajUnos() {
        //definisati try catch blok koji ce uhvatiti greske 
        //napravljene u funkciji proveri unos
        try {
            proveriUnos();
        } catch (err) {
            console.log("Desila se greska u funkciji proveriUnos");
            document.getElementById('console').innerHTML =
                "<h3 style='color:red'> Upozorenje: </h3>" + err.message;
        }
    }

        //ovaj deo ne treba ici u try  blok


    