//#region Task 1

var userNumber = prompt("Guess a random number between 1 and 10"); // Create variable for storing the prompted number

var randomNumber = Math.round(Math.random() * 10); // Create another variable to generate random number

if (userNumber == randomNumber) { // Create if statement to check whether the user guessed the generated random number and alert the results
    alert("Good work, you are AWESOME!");
} else {
    alert("Not matched, " + "the number was " + randomNumber);
}

//#endregion Task 1

//#region Task 2

var sum = 0; // Variable for storing the sum, initial value of 0

// For loop to check which numbers are divisible by 3 and 5, if true, add them to the sum, then log out the result
for (var i = 1; i < 1000; i++) { 
    if (i % 3 === 0 && i % 5 === 0) {
        sum += i;
    }
}
console.log(sum);

//#endregion Task 2

// #region Task 3

// Create variable to store the input numbers 
var numbers = prompt("Unesite vase brojeve"); //Prompt the user to eneter numbers

var string = numbers.toString(); // Create variable to store the numbers converted to string

var result = [string[0]]; // Create an array of input numbers

// Create for loop to check whether the numbers are odd or even, if even push to array with separator "-", otherwise just store it to the array then log out the result.
for (var i = 1; i < string.length; i++) { 
    if ((string[i - 1] % 2 === 0) && (string[i] % 2 === 0)) {
        result.push("-", string[i]);
    } else {
        result.push(string[i]);
    }
}
console.log(result.join(""));

//#endregion Task 3

//#region Task 4

var array = [-3, 8, 7, 6, 5, -4, 3, 2, 1]; // Create an array

// Create a function to sort the numbers
//NOTE for myself: whenever "a" is less than "b", a negative value is returned, which results in the smaller elements always appearing to the left of the larger ones or ascending.
function sortNumbers(a, b) {
    return (a - b);
}

array.sort(sortNumbers); // Apply sort method to array using the function sortNumbers

// Loop through the sorted array and log to console
for (var i in array) {
    console.log(array[i]);
}

//#endregion Task 4

//#region Task 5

var colors = ["Red", "Green", "White", "Black"]; // Create an array

console.log(colors.toString()); // Log to console using toString method

console.log(colors.join()); // Log to console using join method

console.log(colors.join("+")); // Log to console using join method with separator "+"

//#endregion task 5

//#region Additional task

Create an array of students and marks
var students = [
    ["David", 80],
    ["Vinoth", 77],
    ["Divya", 88],
    ["Ishitha", 95],
    ["Thomas", 68]
];

var marks = 0; // Create variable for storing marks (MUST be a type of number, otherwise returns NaN)

// Create for loop to run through the array
for (var i = 0; i < students.length; i++) {
    marks += students[i][1]; // Storing marks in the variable
    var average = (marks / students.length); //calculate the average grade
}
alert("Average grade is " + average);

// Checking the grade through switch statement
switch (true) {
    case (average < 60):
        alert("Grade F")
        break;
    case (average < 70):
        alert("Grade D")
        break;
    case (average < 80):
        alert("Grade C")
        break;
    case (average < 90):
        alert("Grade B")
        break;
    default:
        alert("Grade A")
        break;
}
//#endregion Additional task