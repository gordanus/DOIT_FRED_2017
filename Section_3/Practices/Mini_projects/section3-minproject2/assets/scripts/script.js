//clock function
function startTime() {
    var today = new Date();
    var hours = today.getHours();
    var minutes = today.getMinutes();
    var seconds = today.getSeconds();
    minutes = checkTime(minutes);
    seconds = checkTime(seconds);
    document.getElementById("time").innerHTML = hours + ":" + minutes + ":" + seconds;
   setTimeout(startTime, 1000);

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
}


//end clock function

//validation functions
function proveriIme() {
    var name = document.getElementById("name").value;
    if (name == "") {
        document.getElementById("nameMsg").innerHTML = "Popunite polje sa imenom";
        document.getElementById("name").style.backgroundColor = "yellow";
    } else {
        document.getElementById("nameMsg").innerHTML = "Ime je pravilno popunjeno";
        document.getElementById("name").style.backgroundColor = "white";
        return true;

    }

}

function proveriPrezime() {
    var lastName = document.getElementById("lastName").value;
    if (lastName == "") {
        document.getElementById("lastNameMsg").innerHTML = "Popunite polje sa prezimenom";
        document.getElementById("lastName").style.backgroundColor = "yellow";
    } else {
        document.getElementById("lastNameMsg").innerHTML = "Prezime je pravilno popunjeno";
        document.getElementById("lastName").style.backgroundColor = "white";
        return true;

    }

}

function proveriAdresu() {
    var adress = document.getElementById("adress").value;
    if (adress == "") {
        document.getElementById("adressMsg").innerHTML = "Popunite polje sa adresom";
        document.getElementById("adress").style.backgroundColor = "yellow";
    } else {
        document.getElementById("adressMsg").innerHTML = "Broj telefona je pravilno popunjen";
        document.getElementById("adress").style.backgroundColor = "white";
        return true;

    }

}

function proveriTelefon() {
    var phone = /^[0-9]+$/;
    if (document.getElementById("phone").value == "") {
        document.getElementById("phoneMsg").innerHTML = "Popunite polje sa brojem telefona";
        document.getElementById("phone").style.backgroundColor = "yellow";
    } else if (!document.getElementById("phone").value.match(phone)) {
        document.getElementById("phoneMsg").innerHTML = "Nisu dozvoljena slova";
        document.getElementById("phone").style.backgroundColor = "yellow";
    } else {
        document.getElementById("phoneMsg").innerHTML = "Broj telefona je pravilno popunjen";
        document.getElementById("phone").style.backgroundColor = "white";
        return true;

    }

}

//functions for open and close contact window
function openContact() {
    window.open("kontakt.html", "_blank", "width=400,height=500");
}

function closeContact() {
    window.close();
}
//end functions for open and close contact window

//function for opening commercial at page load
function reklama() {
    window.open("./reklama.html", "_blank", "width=350, height=200");

}
document.body.addEventListener("load", reklama());
//end function for opening commercial at page load

//function for printing page
function stampaj() {
    window.print();
}
printBtn.addEventListener("click", stampaj);
//end function for printing page

//main function for calling all validation functions
function validateAll() {
    proveriIme();
    proveriPrezime();
    proveriAdresu();
    proveriTelefon();
    return true;
   
}
//main function for calling all validation functions

function confirmAll() {
    if (validateAll()) {
        console.log("validacija OK");
        confirm("Da li zelite da posaljete podatke?");
    } else {
        console.log("validacija nije OK");
    }
}

submitBtn.addEventListener("click", confirmAll);