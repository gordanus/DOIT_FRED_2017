$(document).ready(function () {
    var $user = $('#userInput').val();
    
    $('#btn').on ('click', function () {         
        $.ajax({
            type: 'GET',
            url: 'https://api.github.com/users/' + $user,
            dataType: 'json',
            success: function(data, status, xhr) {
                //results je deo data objekta pogledati sa console.log(data) strukturu
                console.log(data);
                console.log(status);

                $('#output').append(data.name + " " + data.login + "<br>" + `<img src = "${data.avatar_url}" alt = "Image not available" width = 100>` + "<br>" + "Followers: " + data.followers + "<br>" + "Following: " + data.following + "<br>" + "Repos: " + data.public_repos);
                
                $.ajax({
                    type: 'GET',
                    url: 'https://api.github.com/users/' + $user + '/repos',  
                    dataType: 'json',
                    success: function (data, status, xhr) {
                        console.log(data);
                        console.log(status);
                        var reposList = [];
                        for (i = 0; i < data.length; i++){
                            reposList.push(`<a href = "${data.repos_url}"></a>`)
                        }

                        $('#repos').append(reposList);
                }
            })    
               
            },
            error: function(xhr, status, error) {
                alert('Desila se greška: ' + status);
            }
            
        });
    });
});