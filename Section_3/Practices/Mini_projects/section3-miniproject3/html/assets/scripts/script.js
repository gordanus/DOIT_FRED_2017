function pokaziVreme() {
    var sad = new Date();
    var sati = sad.getHours();
    var minuti = sad.getMinutes();
    var sekunde = sad.getSeconds();
    if (sekunde <= 9) {
        sekunde = "0" + sekunde;
    }
    if (minuti <= 9) {
        minuti = "0" + minuti;
    }
    if (sati <= 9) {
        sati = "0" + sati;
    }
    // value se koristi za input/form elements. 
    //innerHTML se koristi yza div, span, td itd.
    document.getElementById("sat").innerHTML = (sati + ":" + minuti + ":" + sekunde);
    //document.vreme.sat.value = (sati + ":" + minuti + ":" + sekunde);
    window.setTimeout("pokaziVreme()", 1000);
}

function startujSat() {
    pokaziVreme();
}