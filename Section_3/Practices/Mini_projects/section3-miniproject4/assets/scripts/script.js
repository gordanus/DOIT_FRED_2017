$(document).ready(function () {

    $("#submitBtn").click(function () {
        validateAll();
        
    });

    // $("#inputForm :input").each(function () {
    //     $(this).blur(function () {
    //         validateAll();
    //     });
    // });

    $("#printBtn").click(function () {
        stampaj();
    });



    function validateAll() {

        //Regex variables
        var imeReg = /^[A-Za-z]+$/;
        var prezimeReg = /^[A-Za-z]+$/;
        var telefonReg = /^[0-9]+$/;

        //Input values
        var ime = $("#name").val();
        var prezime = $("#lastName").val();
        var adresa = $("#adress").val();
        var telefon = $("#phone").val();

        var inputValues = [ime, prezime, adresa, telefon]; //Array for holding values P.S. really not necessary

        if (inputValues[0] == "") {
            $("#name").after("<br class = 'invalidInput'><span class = 'invalidInput' style = 'color: red;'>Molimo unesite ime</span>");
        } else if (!imeReg.test(ime)) {
            $("#name").after("<br class = 'invalidInput'><span class = 'invalidInput' style = 'color: red;'>Dozvoljena su samo slova</span>");
        } else {
            $(".invalidInput").hide();
            return true;
        }

        if (inputValues[1] == "") {
            $("#lastName").after("<br class = 'invalidInput'><span class = 'invalidInput' style = 'color: red;'>Molimo unesite prezime</span>");
        } else if (!prezimeReg.test(prezime)) {
            $("#lastName").after("<br class = 'invalidInput'><span class = 'invalidInput' style = 'color: red;'Dozvoljena su samo slova</span>");
        } else {
            $(".invalidInput").hide();
            return true;
        }

        if (inputValues[2] == "") {
            $("#adress").after("<br class = 'invalidInput'><span class = 'invalidInput' style = 'color: red;'>Molimo unesite adresu</span>");
        } else {
            $(".invalidInput").hide();
            return true;
        }

        if (inputValues[3] == "") {
            $("#phone").after("<br class = 'invalidInput'><span class = 'invalidInput' style = 'color: red;'>Molimo unesite broj telefona</span>");
        } else if (!telefonReg.test(telefon)) {
            $("#phone").after("<br class = 'invalidInput'><span class = 'invalidInput' style = 'color: red;'Dozvoljeni su samo brojevi</span>");
        } else {
            $(".invalidInput").hide();
        }
        
    }


});

// function confirmAll() {
//     if (validateAll()) {
//         confirm("Da li zelite da posaljete podatke?")
//     } else {
//         document.reset()
//     }
// }

//functions for open and close contact window
function openContact() {
    window.open("kontakt.html", "_blank", "width=400,height=500");
}

function closeContact() {
    window.close();
}
//end functions for open and close contact window

//function for opening commercial at page load
function reklama() {
    window.open("./reklama.html", "_blank", "width=350, height=200");

}
$("body").on("load", reklama());
//end function for opening commercial at page load

//function for printing page
function stampaj() {
    window.print();
}
//end function for printing page

//clock function
function startTime() {
    var today = new Date();
    var hours = today.getHours();
    var minutes = today.getMinutes();
    var seconds = today.getSeconds();
    hours = checkTime(hours);
    minutes = checkTime(minutes);
    seconds = checkTime(seconds);
    document.getElementById("time").innerHTML = hours + ":" + minutes + ":" + seconds;
    setTimeout(startTime, 1000);

    function checkTime(i) {
        if (i < 10) {
            i = "0" + i;
        }
        return i;
    }
}
//end clock function