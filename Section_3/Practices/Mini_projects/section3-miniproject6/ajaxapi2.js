$(document).ready(function(){
    
    $('#news').change(function () {
        var url;
        var url1;
                
        if ($("#news").val() === "jazeera") {
            url = "https://newsapi.org/v2/top-headlines?sources=al-jazeera-english&apiKey=28f2e4180bd94d2fb594c1eef7b38dcf";
        } else if ($("#news").val() === "bild") {
            url = "https://newsapi.org/v2/top-headlines?sources=bild&apiKey=28f2e4180bd94d2fb594c1eef7b38dcf";
        } else if ($("#news").val() === "cbc") {
            url = "https://newsapi.org/v2/top-headlines?sources=cbc-news&apiKey=28f2e4180bd94d2fb594c1eef7b38dcf";
        } else {
            url = "https://newsapi.org/v2/sources?apiKey=28f2e4180bd94d2fb594c1eef7b38dcf";
        }
         
        $.ajax({
            type: 'GET',
            url: url,
            dataType: 'json',
            success: function(data, status, xhr) {
                //results je deo data objekta pogledati sa console.log(data) strukturu
                console.log(data);
                console.log(status);

                var article = [];
                for(var i = 0; i<data.articles.length;i++){
                    article.push(`<img src = "${data.articles[i].urlToImage}" alt = "Image not available" width = 300>` + "<br>"  + "Title:" + data.articles[i].title + "<br>"+ "Author:" + data.articles[i].author + "<br>"+ "Description:" + data.articles[i].description + "<br>" + "<br>" + `<a href = "${data.articles[i].url}" target = '_blank'>Read more</a>` + "<br>"
                );
                  $('#output').html(article);
                } 
               
            },
            error: function(xhr, status, error) {
                alert('Desila se greška: ' + status);
            }
            
        });
    });
});