//#region Task 1 - SWITCH
// var n = prompt("Enter number between 1 and 7");


// switch (n) {
//     case "1":
//         alert("The value is between 1 and 3 and the set value is " + n);
//         break;
//     case "2":
//         alert("The value is between 1 and 3 and the set value is " + n);
//         break;
//     case "3":
//         alert("The value is between 1 and 3 and the set value is " + n);
//         break;
//     case "5":
//         alert("The value is between 5 and 7 and the set value is " + n);
//         break;
//     case "6":
//         alert("The value is between 5 and 7 and the set value is " + n);
//         break;
//     case "7":
//         alert("The value is between 5 and 7 and the set value is " + n);
//         break;
//     default:
//         alert("Value " + n + " is not within the specified range")
//         break;
// }

//#endregion Task 1

//#region Task 2 - WHILE

// var x=-8;
// var y=3;

// while (x+y<=0) {
//     console.log(x+y)
//     y++
// }

//#endregion Task 2

//#region Task 3 - FOR

// for (i = 1; i < 20; i++) {
//    if (i%4 === 0) {
//        console.log("Number "+ i + " is divisible by 4");
//    }
// }

//#endregion Task 3

//#region Task 4 - FOR + ARRAY

// var towns = ["Novi Sad", "Beograd", "Nis", "Subotica", "Kikinda"];

// for (i=0; i<towns.length; i++){
//     console.log(towns[i]);
// }

//#endregion Task 4

//#region Task 5 - FOR + ARRAY + BREAK

// for (i = 0; i < towns.length; i++) {
//     if (i === towns.indexOf("Subotica")) {
//         break;
//     }
//     console.log(towns[i]);
// }

//#endregion Task 5

//#region Task 6 - FOR + ARRAY + CONTINUE

// for (i = 0; i < towns.length; i++) {
//     if (i === towns.indexOf("Subotica")) {
//         continue;
//     }
//     console.log(towns[i]);
// }

//#endregion Task 6

//#region Task 7 - JavaScript from scratch

// var tempFar=prompt("Enter temperature in Fahrenheit");

// function ConvertToCelsius(tempFar) {
//     return (tempFar - 32) * 5/9;
// }

// alert("The temperature in Celsius is " + ConvertToCelsius(tempFar));


// var tempCel=prompt("Enter temperature in Celsius");

// function ConvertToFahrenheit(tempCel) {
//     return tempCel * 9/5 + 32;
// }

// document.write(`<h2>The temperature in Fahrenheit is ${ConvertToFahrenheit(tempCel)} F</h2>`);


//#endregion Task 7

//#region Task 8 - Functions - split

// function persons(name) {
//     var splitString=name.split("|",3);
//     console.log("Name is: "+splitString[0]);
//     console.log("Surname is: "+splitString[1]);
//     console.log("Date of birth is: "+splitString[2]);
// }

// persons("pera | peric | 24.03.1985.");
// persons("ana | anic | 02.11.1987.");

//#endregion Task 8

//#region Task 9 - Functions – return - substring

function returnStrings(string1, string2){
    var strings = string1.substring(0,5) + string2.substring(4);
    console.log(strings);
}

returnStrings("frontPage", "Backend developer");

