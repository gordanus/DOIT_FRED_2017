//#region Task 1

// Implemented Task 3 and Task 4

var rentACar = {
    name: "GoodCar",
    address: "Bulevar Oslobodjenja 100",
    numberOfCars: 10,
    reservation: 5,
    parking: true,
    rentingPerDay: 100,
    freeCars: function () {
       return (this.numberOfCars-this.reservation);
    },
    increaseCars: function () {
        return (this.reservation + 1);
    },
    calculatePrice: function () {
        return (this.rentingPerDay / 24);
    
    },
    makeReservation: function (){
        var reserve = prompt("Koliko automobila zelite da rezervisete?");
        if (reserve <= this.reservation && reserve > 0){
            alert("Hvala vam! Rezervisali ste " + reserve + " automobil(a)"); 
        }else if (reserve === null || reserve == 0){
           return;
        } else {
            alert ("Nemamo trenutno taj broj vozila na raspolaganju");
        }
    },
    ispisiSve: function () {
        document.write ("Agencija " + this.name + " sa adresom " + this.address +" ima ukupno automobila "+ this.numberOfCars + ", od toga je rezervisano " + this.reservation + " automobila." + "<br>" + "Cena najma automobila je " + this.calculatePrice() + " eura po satu." + "<br>");
        if (this.parking === true) {
            document.write ("Ima slobodnih mesta za parking");
        } else {
            document.write ("Nema slobodnih mesta za parking");
        }
    }
}

// rentACar.ispisiSve();
rentACar.makeReservation();

//#endregion Task 1

//#region Task 2

// var rent1 = {
//         name: "Rent1",
//         address: "Mihajla Pupina 36",
//         numberOfCars: 20,
//         reservation: 6,
//         parking: true
// }

// var BestRent = {
//     name: "BestRent",
//     address: "Fruskogorska 7",
//     numberOfCars: 10,
//     reservation: 5,
//     parking: false
// }

// var agencije = [rent1, BestRent];

// // document.write(rent1);
// for (i=0; i < agencije.length; i++){
//     document.write("Naziv agencije: " + agencije[i].name + "<br> " + "Adresa: " + agencije[i].address + "<br>" + "Broj vozila " + agencije[i].numberOfCars + "<br>" + "Ima parking: " + agencije[i].parking + "<br>" + "<br>");
// }

//#endregion Task 2
