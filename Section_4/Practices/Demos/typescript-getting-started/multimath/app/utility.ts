
    function getInputValue(elementID: string): string{
        let inputElement: any = document.getElementById(elementID);
        console.log(inputElement);
        return inputElement.value;
    }

function logger(message: string): void{
    console.log(message);
}
    
export { getInputValue as getValue, logger };