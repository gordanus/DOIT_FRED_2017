import { IProductService } from "./IProductService";
import { Product } from "./product";
import { Category } from "./category";
import { Rating } from "./rating";



export class ProductService implements IProductService {
    private products: Product[];
    private categories: Category[];
    constructor(products: Product[], categories: Category[]) {
        this.products = products;
        this.categories = categories;
    }
    getCategories() {
        return this.categories;
    }
    getAllProducts() {
        return this.products;
    }
    getProductsByCategory(categoryId: number): Product[] {
        let filteredProducts = this.products.filter(product => product.category === categoryId);
        return filteredProducts;
    }
    addProduct(product: Product) {
        this.products.push(product);
    }
    addRating(productId: number, rating: Rating) {
        let productIndex = this.products.findIndex(product => product.id === productId);
        let product = this.products[productIndex];
        if (product.ratings) {
            product.ratings.push(rating);
        } else {
            product.ratings = [];
            product.ratings.push(rating);
        }
    }
}