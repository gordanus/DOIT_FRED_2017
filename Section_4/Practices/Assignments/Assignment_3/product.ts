import { Rating } from "./rating";

export interface Product extends Rating { 
    id: number;
    name: string;
    originalPrice: number;
    offerPrice?: number;
    stock: number;
    category: number;
    ratings?: Rating[];
}