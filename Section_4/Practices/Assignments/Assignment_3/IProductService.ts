import { Category } from "./category";
import { Product } from "./product";
import { Rating } from "./rating";


export interface IProductService { //extends Category, Product, Rating 
    getCategories:() => Array<Category>;
    getAllProducts:() => Array<Product>;
    getProductsByCategory: (categoryId: number) => Product[];
    addProduct: (product: Product) => void;
    addRating: (productId: number, rating: Rating) => void;
}