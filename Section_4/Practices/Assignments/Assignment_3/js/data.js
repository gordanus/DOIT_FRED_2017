"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CATEGORIES = [
    {
        id: 1,
        name: 'MOBILES'
    },
    {
        id: 2,
        name: 'BAKERY'
    },
    {
        id: 3,
        name: 'FRUITS'
    }
];
exports.PRODUCTS = [
    {
        id: 1,
        name: 'iphone',
        originalPrice: 599,
        stock: 20,
        category: 1,
        ratings: [
            {
                id: 1,
                comment: 'Good phone'
            },
            {
                id: 2,
                comment: 'Awesome'
            },
            {
                id: 3,
                comment: 'Good but pricy'
            }
        ]
    },
    {
        id: 1,
        name: 'bread',
        originalPrice: 10,
        stock: 20,
        category: 2,
        ratings: [
            {
                id: 1,
                comment: 'fresh and Yummy'
            },
            {
                id: 2,
                comment: 'good taste'
            }
        ]
    }
];
//# sourceMappingURL=data.js.map