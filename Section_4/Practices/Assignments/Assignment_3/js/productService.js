"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ProductService = (function () {
    function ProductService(products, categories) {
        this.products = products;
        this.categories = categories;
    }
    ProductService.prototype.getCategories = function () {
        return this.categories;
    };
    ProductService.prototype.getAllProducts = function () {
        return this.products;
    };
    ProductService.prototype.getProductsByCategory = function (categoryId) {
        var filteredProducts = this.products.filter(function (product) { return product.category === categoryId; });
        return filteredProducts;
    };
    ProductService.prototype.addProduct = function (product) {
        this.products.push(product);
    };
    ProductService.prototype.addRating = function (productId, rating) {
        var productIndex = this.products.findIndex(function (product) { return product.id === productId; });
        var product = this.products[productIndex];
        if (product.ratings) {
            product.ratings.push(rating);
        }
        else {
            product.ratings = [];
            product.ratings.push(rating);
        }
    };
    return ProductService;
}());
exports.ProductService = ProductService;
//# sourceMappingURL=productService.js.map