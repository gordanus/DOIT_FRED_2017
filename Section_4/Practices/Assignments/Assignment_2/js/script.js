"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Department = (function () {
    function Department(name) {
        this.name = name;
        this.printName = function (name) { return console.log("Department is " + name); };
        console.log("Department is " + name);
    }
    return Department;
}());
var AccountingDepartment = (function (_super) {
    __extends(AccountingDepartment, _super);
    function AccountingDepartment() {
        return _super.call(this, 'Accounting Department') || this;
    }
    AccountingDepartment.prototype.printMeeting = function (subDepth) {
        document.getElementById('output').innerHTML = "You have a meeting with the " + subDepth + " department at 08:00 IST";
    };
    return AccountingDepartment;
}(Department));
var BioTechnologyDepartment = (function (_super) {
    __extends(BioTechnologyDepartment, _super);
    function BioTechnologyDepartment() {
        return _super.call(this, 'BioTechnology and Genetics Department') || this;
    }
    BioTechnologyDepartment.prototype.printMeeting = function (subDepth) {
        document.getElementById('output2').innerHTML = "You have a meeting with the " + subDepth + " department at 14:00 IST";
    };
    return BioTechnologyDepartment;
}(Department));
var accDep = new AccountingDepartment();
var bioDep = new BioTechnologyDepartment();
var btn = document.getElementById("button");
btn.addEventListener('click', function () {
    accDep.printMeeting('Finance');
    bioDep.printMeeting('Cellbiology');
});
//# sourceMappingURL=script.js.map