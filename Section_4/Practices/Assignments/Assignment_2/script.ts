//#region Task 1

// class Animal {
//     constructor(public name: string | any) {
//         this.name = name;
//     }

//     move(distanceInMeter: number = 0): void {
//         console.log(`${this.name} the Horse moved ${distanceInMeter}`);
        
//     }

//     behave(trait: string) {
//         console.log(`${this.name} the Pyton does a ${trait}`);

//     }
// }

// class Snake extends Animal {
//     constructor(public name: string | any) {
//         super(name); 
//         this.name = name;
        
//     }
//     move(distanceInMeter: number = 5): void {
//         console.log(`My  + ${this.name}+ Is Slithering`);
//         super.move(distanceInMeter);
//     }
    
//     behave(trait: string = "Hiss") {
//         console.log('Hissing SCARILY!!');
//         super.behave(trait);

//     }
// }

// class Horse extends Animal {
//     constructor(name: string) {
//         super(name);
//     }
//     move(distanceInMeter: number = 45): void {
//         console.log('That Horse Is Galloping!!');
        
//         // console.log(`'That ' + ${this.name}+' Is Galloping!'`);
//         super.move(distanceInMeter);
//     }
 
// }

// let snake = new Snake('Sammy');
// let horse = new Horse('Jimmy');

// snake.behave();
// horse.move();

//#endregion Task 1

//#region task 2

abstract class Department {
    constructor( public name: string) {
        console.log(`Department is ${name}`);
    }
    printName = (name: string): void => console.log(`Department is ${name}`);

    abstract printMeeting(subDepth: string): void
}

class AccountingDepartment extends Department {
    constructor() {
        super('Accounting Department');
    }
    printMeeting(subDepth: string) {
       document.getElementById('output')!.innerHTML = `You have a meeting with the ${subDepth} department at 08:00 IST`;
       
   }
}

class BioTechnologyDepartment extends Department {
    constructor() {
        super('BioTechnology and Genetics Department');
    }
    printMeeting(subDepth: string) {
        document.getElementById('output2')!.innerHTML =`You have a meeting with the ${subDepth} department at 14:00 IST`;
    }
}

const accDep = new AccountingDepartment();
// accDep.printMeeting('Finance');

const bioDep = new BioTechnologyDepartment();
// bioDep.printMeeting('Cellbiology');

let btn: any = document.getElementById("button");

    btn.addEventListener('click', () => {
        accDep.printMeeting('Finance');
        bioDep.printMeeting('Cellbiology');
});