//#region Zadatak 1

// let myString: string = "Gordan";
// let myNumber: number = 10;
// let myBool: boolean = true;

// function Ispisi(param1: string, param2: number, param3: boolean): void {

//     if (myBool === true) {
//         console.log('String je: ' + myString + ' a broj je: ' + myNumber.toString());
//     } else {
//         console.log("Boolean vrednost je false!");

//     }

// };

// Ispisi(myString, myNumber, myBool);

//#endregion Zadatak 1

//#region Zadatak 2

// let niz1: number[] = [1, 2, 3, 4, 5];
// let niz2: number[] = [6, 7, 8, 9, 10];

// function spojiIispisi(): void {
//     let niz3 = [...niz1, ...niz2];
//     console.log(niz3.toString());
// }

// function ispisiISpoji(): void {
//     let niz3 = [...niz2, ...niz1];
//     console.log(niz3.toString());

// }

let osoba1 = {
    ime: "Petar",
    prezime: "Petrovic",
    godine:30
}

let osoba2 = {
    ime: "Dragana",
    prezime: "Draganic",
    godine:35
}
function spojiDvoje(): void {
    let dveOsobe = [{ osoba1 }, {osoba2 }]
    console.log(dveOsobe);

}    

// spojiIispisi();
// ispisiISpoji();
spojiDvoje();

//#endregion Zadatak 2

//#region Zadatak 3

// interface Hrana{
//     naziv: string;
//     kalorijskaVrednost: number;
// }

// function ispisiVrednosti(param: Hrana): void {
//     console.log(param.naziv);
//     console.log(param.kalorijskaVrednost);
// }

// // let sveZajedno = {
// //     naziv: "Pizza",
// //     kalorijskaVrednost:500
// // }

// ispisiVrednosti({
//         naziv: "Pizza",
//         kalorijskaVrednost:500
//     });

//#endregion Zadatak 3

//#region Zadatak 4

// class Menu {
//     // jela: string [];
//     // brojStrana: number;
//     constructor(public jela: string[]| any, public brojStrana: number) {
//         this.jela = jela;
//         this.brojStrana = brojStrana;
//         console.log('Meni ima '+ this.brojStrana+' strana.');
//     }
//     listMenu() {
//         for (let i = 0; i < this.jela.length; i++) {
//             console.log(this.jela[i]);
//         }
//     }
    
    
// }

// const noviMeni = new Menu(['corbe', 'spagete', 'rostilj', 'pizza'], 15);
// noviMeni.listMenu();

//#region Zadatak 5

// class Menu {

//     constructor(public jela: string[]| any, public brojStrana: number) {
//         this.jela = jela;
//         this.brojStrana = brojStrana;
//         console.log('Meni ima '+ this.brojStrana+' strana.');
//     }
//     listMenu() {
//         for (let i = 0; i < this.jela.length; i++) {
//             console.log(this.jela[i]);
//         }
//     }
    
    
// }

// class DnevniMeni extends Menu {
    
// }

// const noviMeni = new Menu(['corbe', 'spagete', 'rostilj', 'pizza'], 15);
// noviMeni.listMenu();

// const noviDnevniMeni = new DnevniMeni(['kuvana jela', 'gotova jela'], 20);
// noviDnevniMeni.listMenu();


