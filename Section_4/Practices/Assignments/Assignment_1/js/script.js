"use strict";
var osoba1 = {
    ime: "Petar",
    prezime: "Petrovic",
    godine: 30
};
var osoba2 = {
    ime: "Dragana",
    prezime: "Draganic",
    godine: 35
};
function spojiDvoje() {
    var dveOsobe = [{ osoba1: osoba1 }, { osoba2: osoba2 }];
    console.log(dveOsobe);
}
spojiDvoje();
//# sourceMappingURL=script.js.map