import { Ispit } from "./ispit";

export class IspitniRok implements Ispit{
    constructor(public brojIndeksa: number, public ime: string, public prezime: string, public sifraPredmeta: number, public nazivPredmeta: string) {
        this.brojIndeksa = brojIndeksa;
        this.ime = ime;
        this.prezime = prezime;
        this.sifraPredmeta = sifraPredmeta;
        this.nazivPredmeta = nazivPredmeta;
    }
    ispisiSve() {
        console.log(`Student: ${this.ime} ${this.prezime} sa brojem indeksa ${this.brojIndeksa} polaze ${this.nazivPredmeta} sa sifrom predmeta ${this.sifraPredmeta}`);
        
    }
}