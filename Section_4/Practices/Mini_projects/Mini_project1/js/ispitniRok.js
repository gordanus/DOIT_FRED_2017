"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var IspitniRok = (function () {
    function IspitniRok(brojIndeksa, ime, prezime, sifraPredmeta, nazivPredmeta) {
        this.brojIndeksa = brojIndeksa;
        this.ime = ime;
        this.prezime = prezime;
        this.sifraPredmeta = sifraPredmeta;
        this.nazivPredmeta = nazivPredmeta;
        this.brojIndeksa = brojIndeksa;
        this.ime = ime;
        this.prezime = prezime;
        this.sifraPredmeta = sifraPredmeta;
        this.nazivPredmeta = nazivPredmeta;
    }
    IspitniRok.prototype.ispisiSve = function () {
        console.log("Student: " + this.ime + " " + this.prezime + " sa brojem indeksa " + this.brojIndeksa + " polaze " + this.nazivPredmeta + " sa sifrom predmeta " + this.sifraPredmeta);
    };
    return IspitniRok;
}());
exports.IspitniRok = IspitniRok;
//# sourceMappingURL=ispitniRok.js.map